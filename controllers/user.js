const User = require("../models/user");
const Order = require("../models/order");
const { check, validationResult } = require("express-validator");

exports.getUserById = (req, res, next, id) => {
  console.log("Inside getUseryID");
  User.findById(id).exec((err, user) => {
    if (err || !user) {
      return res.status(400).json({
        error: "No user found",
      });
    }

    req.profile = user;
    next();
  });
};

exports.getUser = (req, res) => {
  req.profile.encry_password = undefined;
  req.profile.salt = undefined;
  req.profile.createdAt = undefined;
  req.profile.updatedAt = undefined;
  return res.json(req.profile);
};

//TEST METHOD
exports.getAllusers = (req, res) => {
  User.find().exec((err, users) => {
    if (err || !users) {
      return res.status(400).json({
        error: "No data found",
      });
    }
    res.json({
      users,
    });
  });
};

exports.updateUser = (req, res) => {
  User.findByIdAndUpdate(
    { _id: req.profile._id },
    { $set: req.body },
    { new: true, useFindAndModify: true },
    (err, user) => {
      if (err || !user) {
        return res.status(400).json({
          error: "cannot update",
        });
      }

      user.encry_password = undefined;
      user.salt = undefined;

      return res.json(user);
    }
  );
};

exports.updatePassword = (req, res) => {
  User.findById({ _id: req.profile._id }, (err, user) => {
    if (err || !user) {
      return res.status(400).json({
        error: "cannot find Id",
      });
    }
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).json({
        error: errors.array()[0].msg,
      });

    user.password = req.body.password;
    user.save((err, user) => {
      if (err) {
        console.log(err);
        return res.status(400).json({ err: "error in updating password" });
      }
    });

    return res.json({ message: "updated password" });
  });
};

exports.userPurchaseList = (req, res) => {
  Order.find({
    user: req.profile._id,
  })
    .populate("user", "_id name")
    .exec((err, order) => {
      if (err) {
        return res.status(400).json({
          error: "No order in this account",
        });
      }
      return res.json({ order });
    });
};

exports.pushOrderInPurchaseList = (req, res, next) => {
  let purchases = [];
  req.body.order.products.array.forEach((product) => {
    purchases.push({
      _id: product._id,
      name: product.name,
      description: product.description,
      category: product.category,
      quantity: product.quantity,
      amount: req.body.order.amount,
      transaction_id: req.body.order.transaction_id,
    });
  });
  User.findOneAndUpdate(
    { _id: req.profile._id },
    { $push: { purchases: purchases } },
    { new: true },
    (err, purchase) => {
      if (err) {
        return res.status(400).json({
          error: "Unable to save purchases",
        });
      }
      res.status(200).json({
        message: "Saved",
      });
      next();
    }
  );
  next();
};

const User = require("../models/user");
const { validationResult } = require("express-validator");
var jwt = require("jsonwebtoken");
var expressJwt = require("express-jwt");
exports.signout = (req, res) => {
  //   res.send("user signout");
  res.clearCookie("token");
  res.json({
    message: "user signed out ",
  });
};
exports.signup = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({
      error: errors.array()[0].msg,
      parameter: errors.array()[0].param,
    });
  console.log(req.body);
  const user = new User(req.body);
  user.save((err, user) => {
    if (err) {
      console.log(err);
      return res.status(400).json({ err: "error in saving user" });
    }
    res.json({
      name: user.name,
      email: user.email,
    });
  });
};

exports.signin = (req, res) => {
  const { email, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({
      error: errors.array()[0].msg,
    });

  User.findOne({ email }, (err, user) => {
    if (err || !user)
      return res.status(400).json({
        error: "Email Id does not exist",
      });
    if (!user.authenticate(password)) {
      return res.status(400).json({
        error: "password doesn't match",
      });
    }

    const token = jwt.sign({ _id: user._id }, process.env.SECRET);
    res.cookie("token", token, { expire: new Date() + 999 });

    const { _id, name, email, role } = user;
    res.status(200).json({
      message: "user authenticated",
      token: token,
      user: {
        _id,
        name,
        email,
        role,
      },
    });
  });
};

//protected routes

exports.isSignedIn = expressJwt({
  secret: process.env.SECRET,
  userProperty: "auth",
});

exports.isAuthenticated = (req, res, next) => {
  let checker = req.profile && req.auth && req.profile._id == req.auth._id;
  console.log("ina auth");
  if (!checker) {
    return res.status(403).json({
      error: "ACCESS DENIED",
    });
  }
  next();
};

exports.isAdmin = (req, res, next) => {
  console.log("is adm");
  if (req.profile.role === 0) {
    return res.status(403).json({
      error: "You are not ADMIN, Access denied",
    });
  }
  next();
};

const { check, validationResult } = require("express-validator");
var express = require("express");
var router = express.Router();

const { signout, signup, signin, isSignedIn } = require("../controllers/auth");

router.get("/signout", signout);

router.post(
  "/signup",
  [
    check("name", "name.len() >3").isLength({ min: 3 }),
    check("email", "invalid email").isEmail(),
    check("password", "pass.len() >3").isLength({ min: 3 }),
  ],
  signup
);

router.post(
  "/signin",
  [
    check("email", "invalid email").isEmail(),
    check("password", "password is required").isLength({ min: 3 }),
  ],
  signin
);

router.get("/testroute", isSignedIn, (req, res) => {
  console.log(req);
  res.send("protected route");
});
module.exports = router;

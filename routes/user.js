const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");

const {
  getUserById,
  getUser,
  getAllusers,
  updateUser,
  updatePassword,
  userPurchaseList,
} = require("../controllers/user");
const { isSignedIn, isAuthenticated, isAdmin } = require("../controllers/auth");

router.param("userId", getUserById);

router.get("/user/:userId", isSignedIn, isAuthenticated, getUser);
router.get("/users", getAllusers);

router.put("/user/:userId", isSignedIn, isAuthenticated, updateUser);
router.get("/user/:userId", isSignedIn, isAuthenticated, userPurchaseList);

router.put(
  "/user/updatePassword/:userId",
  [check("password", "pass.len() >3").isLength({ min: 3 })],
  isSignedIn,
  isAuthenticated,
  updatePassword
);
module.exports = router;
